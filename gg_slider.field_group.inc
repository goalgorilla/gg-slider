<?php
/**
 * @file
 * gg_slider.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gg_slider_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slide_common|node|slide|form';
  $field_group->group_name = 'group_slide_common';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Algemeen',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_slide_link',
      2 => 'title',
      3 => 'field_slide_readmore',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_slide_common|node|slide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slide_media_image|node|slide|form';
  $field_group->group_name = 'group_slide_media_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_slide_media';
  $field_group->data = array(
    'label' => 'Afbeelding',
    'weight' => '3',
    'children' => array(
      0 => 'field_slide_media_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Afbeelding',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'mediatype',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_slide_media_image|node|slide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slide_media_video|node|slide|form';
  $field_group->group_name = 'group_slide_media_video';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_slide_media';
  $field_group->data = array(
    'label' => 'Video',
    'weight' => '4',
    'children' => array(
      0 => 'field_slide_media_video',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Video',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'mediatype',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_slide_media_video|node|slide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slide_media|node|slide|form';
  $field_group->group_name = 'group_slide_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Media',
    'weight' => '1',
    'children' => array(
      0 => 'field_slide_media',
      1 => 'group_slide_media_image',
      2 => 'group_slide_media_video',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_slide_media|node|slide|form'] = $field_group;

  return $export;
}
