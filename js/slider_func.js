jQuery(document).ready(function($) {
 	// Enable the API on each Vimeo video
  $('iframe.vimeo').each(function() {
    $f(this).addEvent('ready', ready);
  });
  
  function ready(player_id) {
    froogaloop = $f(player_id);

    function setupEventListeners() {
      function onPlay() {
        froogaloop.addEvent('play', function(data) {
          // Stop cycling.
          $('#views_slideshow_controls_text_pause_gg_slideshow-gg_slideshow_block').click().click();
          $('#views_slideshow_cycle_main_gg_slideshow-gg_slideshow_block .node.video:visible h2').hide();
        });
      }

      function onPause() {
        froogaloop.addEvent('pause', function(data) {
          // Start cycling.
          $('#views_slideshow_controls_text_pause_gg_slideshow-gg_slideshow_block').click();
          $('#views_slideshow_cycle_main_gg_slideshow-gg_slideshow_block .node.video:visible h2').show();
        });
        }

      function onFinish() {
        froogaloop.addEvent('finish', function(data) {
          // Start cycling.
          $('#views_slideshow_controls_text_pause_gg_slideshow-gg_slideshow_block').click();
        });
      }

      onPlay();
      onPause();
      onFinish();
    }
    setupEventListeners();
  }
});

/**
 * This function corresponds with the parameter set in the settings for slideshow (GG Slideshow view).
 * It is a transition callback. So if the slide moves to the previous or next slide we pause any playing video's.
 */
Drupal.ggSlideshowBefore = function (currSlideElement, nextSlideElement, options, forwardFlag) {
  jQuery('iframe.vimeo').each(function() {
    if ($f(this)) {
      $f(this).api('pause');
    }
  });
}