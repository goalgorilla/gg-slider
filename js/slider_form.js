(function($) {
/**
 * Provides conditional fields functionality for the slider form.
 */
Drupal.behaviors.gg_slider = {
  attach: function (context, settings) {
    /* At first, we will hide the fieldsets and the n/a radio.
       We also add a required asterix to the input fields. */
    $('fieldset#node_slide_form_group_slide_media_image, fieldset#node_slide_form_group_slide_media_video').hide();
    $('input#edit-field-slide-media-und-none').parent().remove();
    $('fieldset.mediatype div.form-item label').append('<span class="form-required" title="This field is required.">*</span>');
    
    /* Secondly, we'll check if there's already a type of media selected and show the fieldset accordingly. */
    var selected = $('div#edit-field-slide-media input[type=radio]:checked').val();
    $('fieldset#node_slide_form_group_slide_media_'+ selected).show();
    
    /* Thirdly, we'll enable the show/hide change event. */
    $('div#edit-field-slide-media input[type=radio]').change(function() {
      var mediatype = $(this).val();
      
      $('fieldset.mediatype').not('.group-slide-media-'+ mediatype).hide();
      $('fieldset#node_slide_form_group_slide_media_'+ mediatype).show();
    });
    
    /* Last but not least, error checking! */
    var errors = [];
    $('input#edit-submit, input#edit-preview').click(function() {
      // If there is a radio checked, proceed with error checking.
      if ($('div#edit-field-slide-media input[type=radio]:checked').size() > 0) {
        // Clear error messages first.
        $('fieldset.mediatype div.form-item input').removeClass('error');
        
        var mediatype = $('div#edit-field-slide-media input[type=radio]:checked').val();
        
        // Check if the input is empty.
        if (!$('div#edit-field-slide-media-'+ mediatype +' input').val()) {
          // If the error is not on screen already.
          if ($.inArray(mediatype, errors) == -1) {
            // Let the user know the input field contains errors.
            $('div#edit-field-slide-media-'+ mediatype +' input').addClass('error');
            
            // Check if there are error messages visible already.
            if ($('div.messages.error').length == 0) {
              $('h1.title').after('<div class="messages error"><h2 class="element-invisible">Error message</h2>'+ mediatype.charAt(0).toUpperCase() + mediatype.slice(1) +' field is required.</div>');
            }
            else {
              $('div.messages.error').append('<br />'+ mediatype.charAt(0).toUpperCase() + mediatype.slice(1) +' field is required.');
            }
            
            // Push the error in the array.
            errors.push(mediatype);
          }
          
          // Prevent submission of the form.
          return false;
        }
      }
    });
  }
}
})(jQuery);